# Bing search with Selenium

Note that this program uses FireFox driver.

## To run on your local machine

install the dependencies and run either `windows.py` or `linux.py` depending on your OS

```bash
python3 -m venv venv
source ./venv/bin/activate
pip install -r requirements
python3 linux.py
```

create file `variables.py` and add the path of your desired Firefox profile Something like: `/home/YOUR_USERNAME/.mozilla/firefox/abcdef.profile`.
note that if using `--notify` you need ntfy, the auth is Basic
```py
profile_path = "/path/to/firefox/profile"
ntfy = {"url": "https://ntfy.sh", "topic": "a-bing-topic", "auth": "dXNlcjpwYXNzd29yZA=="}
```
