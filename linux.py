"""
TODO: add readme,  about:config  general.useragent.override: "" 
TODO: Fix the deprecated function for choosing 'Firefox Profile'
TODO: Use multi-thread to run concurrently
TODO: show word list -> -v --verbose
"""

import argparse
import time
from random import uniform
import platform

import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager

import variables as var
from update_user_agent import get_latest_user_agent, update_latest_user_agent


def search_random_word(web_driver, iterations: int, url: str, input_box_element: str) -> None:
    web_driver.get(url)

    response = requests.get(f'https://random-word-api.herokuapp.com/word?number={iterations}')
    # // response = requests.get(f'https://random-word-form.herokuapp.com/random/noun/a?count={iterations}')
    word_list = response.json()

    print('-----------------------------')
    print("word list:")
    print('\n'.join(word_list))
    print('-----------------------------')

    for word in word_list:
        # Finding input element on page and wait if it is not yet loaded
        search_input_element = WebDriverWait(web_driver, timeout=3).until(lambda d: d.find_element(By.ID, input_box_element))
        try:
            search_input_element.clear()
        except Exception as err:
            print("ERROR:", err)
            web_driver.refresh()

        search_input_element.send_keys(word)
        search_input_element.send_keys("\ue007")  # ENTER KEY: https://www.selenium.dev/documentation/webdriver/actions_api/keyboard/

        time.sleep(round(uniform(4.4, 7.7), 1))

    time.sleep(round(uniform(3.3, 5.5), 1))
    web_driver.refresh()

    time.sleep(round(uniform(2.2, 4.4), 1))


def get_options(user_agent: str, is_mobile: bool) -> Options:
    options = Options()

    # * https://www.selenium.dev/documentation/webdriver/browsers/firefox/#profiles
    # https://stackoverflow.com/questions/69571950/deprecationwarning-firefox-profile-has-been-deprecated-please-pass-in-an-optio
    # options.set_preference("profile", var.profile_path) # it doesn't set profile
    options.profile = var.profile_path

    # firefox_options.add_argument("-profile")
    # firefox_options.add_argument(var.profile_path)

    if not is_mobile:
        options.set_preference('general.useragent.override', user_agent)

    elif is_mobile:

        options.add_argument('--width=360')
        options.add_argument('--height=640')
        options.add_argument('--log-level=0')

        options.set_preference('general.useragent.override', user_agent)
    else:
        print("Device is unexpected, unable to continue")
        exit(0)
    return options


def get_points(web_driver):
    web_driver.get("https://www.bing.com/")
    time.sleep(1)
    web_driver.refresh()
    time.sleep(1)
    return web_driver.find_element(By.ID, "id_rc").text


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog="Bing Search", description="Search bing with selenium!")
    parser.add_argument('--version', action='version', version='Bing Search 1.0')

    parser.add_argument("iterations", nargs='?', help="how many search to perform (Default: 35)", default=35, type=int)
    parser.add_argument('-b', '--browser', help="launches firefox browser with the selected profile", action='store_true')
    parser.add_argument("-m", "--mobile", help="search Bing with mobile UA", action='store_true')
    parser.add_argument("-d", "--desktop", help="search Bing with desktop UA", action='store_true')
    # TODO: 1 arg for max, min and none
    parser.add_argument("--maximize", help="Desktop search instance will be maximized", action='store_true')
    parser.add_argument("--minimize", help="all instance be minimized", action='store_true')
    parser.add_argument("-a", "--all", help="search Bing with desktop and mobile UA and launches browser", action='store_true')
    parser.add_argument("-s", "--search", help="search Bing with desktop and mobile UA", action='store_true')
    parser.add_argument("-u", "--url", help="URL to open when launching browser", action='store', default="https://www.bing.com")
    parser.add_argument("--update", help="downloads the latest edge UA from: whatismybrowser.com", action="store_true")
    parser.add_argument("-n", "--notify", help="send notification via ntfy.sh", action="store_true")

    args = parser.parse_args()
    run_browser = args.browser
    run_mobile = args.mobile
    run_desktop = args.desktop
    run_maximized = args.maximize

    notify_message = {
        "before": "0",
        "after": "0",
    }

    if args.all is True:
        run_browser, run_mobile, run_desktop = True, True, True

    if args.search is True:
        run_mobile, run_desktop = True, True

    repeat_num = args.iterations

    if args.update:  # if --update is used
        update_latest_user_agent()

    # If user agent file does not exist then create it
    try:
        dict_user_agent = get_latest_user_agent()
    except FileNotFoundError:
        update_latest_user_agent()
        dict_user_agent = get_latest_user_agent()

    mobile_user_agent = dict_user_agent['mobile']
    windows_user_agent = dict_user_agent['desktop']

    # Get CPU Architecture to download the correct driver
    # TODO: Convert to switch case and add statement for linux intel 32 bit
    os_architecture = None
    if platform.machine() == "x86_64":
        os_architecture = "linux64"
    elif platform.machine() == "aarch64":
        os_architecture = "linux-aarch64"
    else:
        print("CPU not supported")
        exit(0)
    
    # get current points for notification 
    if args.notify:
        print("Checking current points")
        desktop_web_driver = webdriver.Firefox(service=Service(GeckoDriverManager(os_type=os_architecture).install()), options=get_options(windows_user_agent, False))
        notify_message["before"] = get_points(desktop_web_driver)
        desktop_web_driver.quit()
    
    # Run Desktop search
    if run_desktop:
        desktop_web_driver = webdriver.Firefox(service=Service(GeckoDriverManager(os_type=os_architecture).install()), options=get_options(windows_user_agent, False))

        # Run maximized if --maximize else run minimized
        desktop_web_driver.maximize_window() if run_maximized else desktop_web_driver.minimize_window()

        print("Running Desktop Search")
        search_random_word(
            web_driver=desktop_web_driver,
            iterations=repeat_num,
            url="https://www.bing.com/",
            input_box_element="sb_form_q")
        desktop_web_driver.quit()

    if run_mobile:
        mobile_web_driver = webdriver.Firefox(service=Service(GeckoDriverManager(os_type=os_architecture).install()), options=get_options(mobile_user_agent, True))

        print("Running Mobile Search")
        search_random_word(
            web_driver=mobile_web_driver,
            iterations=repeat_num,
            url="https://www.bing.com/",
            input_box_element="sb_form_q")
        mobile_web_driver.quit()

    # get new points for notification
    if args.notify:
        print("Checking new points")
        desktop_web_driver = webdriver.Firefox(service=Service(GeckoDriverManager(os_type=os_architecture).install()), options=get_options(windows_user_agent, False))
        notify_message["after"] = get_points(desktop_web_driver)
        desktop_web_driver.quit()

        threshold = 162
        did_points_meet_the_threshold = (int(notify_message['before']) + threshold) == int(notify_message['after'])

        # Send notification
        requests.post(f"{var.ntfy['url']}/{var.ntfy['topic']}",
        data=f"Points: {notify_message['before']} ➡️ {notify_message['after']}  // {int(notify_message['after']) - int(notify_message['before'])}".encode(encoding='utf-8'),
        headers={
        "Title": "Result Status [Bing-search]",
        "Priority": "low" if did_points_meet_the_threshold else "default",
        "Tags": "white_check_mark" if did_points_meet_the_threshold else "warning",
        "Authorization": "Basic " + var.ntfy["auth"]
    })

    if run_browser:
        browser_driver = webdriver.Firefox(service=Service(GeckoDriverManager(os_type=os_architecture).install()), options=get_options(windows_user_agent, False))
        browser_driver.get(args.url)
        browser_driver.maximize_window()
