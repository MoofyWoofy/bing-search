from bs4 import BeautifulSoup
import requests
import os


def get_user_agent_file_location() -> str:
    """ returns the location of the user_agents.txt file which is in the same directory as the python script """
    # get the path for this directory
    dir_path = os.path.dirname(__file__)
    # returns the location adding the slash / for *nix and \ for Windows
    return os.path.join(dir_path, 'user_agents.txt')


def update_latest_user_agent():
    print("=== updating user agent ===")
    # Get raw html data
    response = requests.get("https://www.whatismybrowser.com/guides/the-latest-user-agent/edge")  # https://www.useragents.me
    soup = BeautifulSoup(response.content, 'html.parser')

    # parse raw html into list
    user_agents_html = soup.select("table.listing-of-useragents .code")

    desktop_agent_html = user_agents_html[0]

    # There are multiple sample user agents
    mobile_agents_html = user_agents_html[2:6]
    # set the first element as the agent
    mobile_agent_html = mobile_agents_html[0]

    # Writing user agent to file
    with open(get_user_agent_file_location(), 'w') as f:
        f.writelines([f"{desktop_agent_html.text}\n", f"{mobile_agent_html.text}"])

    print("=== user agents updated === ")


def get_latest_user_agent() -> dict:
    """ returns a dictionary/map of user agents """
    with open(get_user_agent_file_location()) as f:
        user_agents = f.read().split('\n')
        return {
            'desktop': user_agents[0],
            'mobile': user_agents[1],
        }
