"""
Requirements:

selenium==4.8.0
webdriver-manager==3.8.5
requests==2.28.2
"""

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.edge.service import Service as EdgeService
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from selenium.webdriver.edge.options import Options
from selenium.webdriver.support.wait import WebDriverWait
import time
import requests


def search_random_word(web_driver, iterations: int, url: str, input_box_element: str):
    web_driver.get(url)
    # web_driver.implicitly_wait(2)  # NOT CORRECT

    response = requests.get(f'https://random-word-api.herokuapp.com/word?number={iterations}')
    word_list = response.json()

    print('-----------------------------')
    print("word list:")
    print('\n'.join(word_list))
    print('-----------------------------')

    for word in word_list:
        search_input_element = WebDriverWait(web_driver, timeout=3).until(lambda d: d.find_element(By.ID, input_box_element))
        # search_input_element = get_element_by_id(web_driver, "sb_form_q")
        search_input_element.clear()

        search_input_element.send_keys(word)
        search_input_element.send_keys("\ue007")  # ENTER KEY: https://www.selenium.dev/documentation/webdriver/actions_api/keyboard/

        time.sleep(3)

    time.sleep(1.5)
    web_driver.refresh()

    time.sleep(5)
    web_driver.quit()

    input("===================="
          "\nProgram has ended... Press \"ENTER\" to exit!\n"
          "====================")


if __name__ == '__main__':
    repeat_num = input('number of times to repeat (default 35): ')

    edge_options = Options()  # TESTING
    # edge_options.add_experimental_option("detach", True)  # TESTING

    web_driver_edge = webdriver.Edge(service=EdgeService(EdgeChromiumDriverManager().install()), options=edge_options)
    web_driver_edge.maximize_window()

    if repeat_num == '-1':
        exit(0)

    search_random_word(
        web_driver=web_driver_edge,
        iterations=repeat_num if repeat_num.isdecimal() else 35,  # IT CAN BE DONE 35 times
        url="https://www.bing.com/",
        input_box_element="sb_form_q")

"""
> pyinstaller --onefile -c windows.py
"""
